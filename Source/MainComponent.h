/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "deps/SerialPort/SerialPort.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public SerialPort::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    
    //==============================================================================
    void serialDataRecieved (const std::string& /*portName*/, const char* /*bytes*/, size_t /*size*/) override
    {
 //       std::string s (bytes, size);
 //       for (size_t i = 0; i < size; i++)
 //           DBG (portName << ": " << bytes[i]);
    }
    
    void serialConnectionStateChanged (const std::string& portName, bool isConnected, const std::string& errorMessage) override
    {
        DBG ("serialConnectionStateChanged: Connected: " << (int)isConnected << "ErrorMessage: "<< errorMessage);
    }
    
    void serialPortTimeout() override
    {
        DBG ("serialPortTimeout");
    }
private:
    SerialPort serial;
    std::string serialPortName;
    //==============================================================================
    // Your private member variables go here...


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
