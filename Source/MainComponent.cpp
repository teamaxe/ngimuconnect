/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "deps/Oscpp/OscMessage.h"
//==============================================================================
MainComponent::MainComponent()
{
    setSize (600, 400);
    auto devices = SerialPort::getDevicePathList();
   // std::string d ("COM10");
    for (auto& d : devices)
    {
        DBG(d);
        #if defined(_WIN64) || defined(_WIN32)
        if (d.find ("COM") != std::string::npos)
        #else   
        if (d.find ("usbmodem") != std::string::npos)
        #endif
        {
            serialPortName = d;
            DBG (serialPortName);
            serial.addListener (this);
            serial.openPort (serialPortName);
            
            Timer::callAfterDelay (1000, [port = serialPortName, this]()
                                        {
                                            OscMessage message ("/mute");
                                            std::vector<char> encodedMessage (message.getEncodedSize());
                                            const size_t encodedSize = message.encode (encodedMessage.data(), encodedMessage.size());
                                            jassert (encodedSize == message.getEncodedSize());
                                            
                                            const size_t slipSize = message.getSlipEncodedSize (encodedMessage.data(), encodedMessage.size());
                                            std::vector<char> slipEncodedMessage (slipSize);
                                            const size_t slipEncodedSize = message.slipEncode (encodedMessage.data(), encodedMessage.size(), slipEncodedMessage.data());
                                            jassert (slipEncodedSize == slipSize);
                                            
                                            serial.writeToPort (port, slipEncodedMessage.data(), slipEncodedMessage.size());
                                            serial.writeToPort (port, slipEncodedMessage.data(), slipEncodedMessage.size());
                                        });
            
            Timer::callAfterDelay (5000, [port = serialPortName, this]()
                                   {
                                       OscMessage message ("/unmute");
                                       std::vector<char> encodedMessage (message.getEncodedSize());
                                       const size_t encodedSize = message.encode (encodedMessage.data(), encodedMessage.size());
                                       jassert (encodedSize == message.getEncodedSize());
                                       
                                       const size_t slipSize = message.getSlipEncodedSize (encodedMessage.data(), encodedMessage.size());
                                       std::vector<char> slipEncodedMessage (slipSize);
                                       const size_t slipEncodedSize = message.slipEncode (encodedMessage.data(), encodedMessage.size(), slipEncodedMessage.data());
                                       jassert (slipEncodedSize == slipSize);
                                       
                                       serial.writeToPort (port, slipEncodedMessage.data(), slipEncodedMessage.size());
                                       serial.writeToPort (port, slipEncodedMessage.data(), slipEncodedMessage.size());
                                   });
        }
    }
}

MainComponent::~MainComponent()
{
    if (serial.isReading())
        serial.closeAllPorts();
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setFont (Font (16.0f));
    g.setColour (Colours::white);
    g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}
