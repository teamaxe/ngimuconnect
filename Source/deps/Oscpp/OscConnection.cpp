//
//  OscConnection.cpp
//  OscSerialTests
//
//  Created by Tom Mitchell on 28/06/2018.
//
//

#include "OscConnection.h"

void OscConnection::addListener (OscConnection::Listener *listenerToAdd)
{
    if (listenerToAdd != nullptr)
    {
        std::lock_guard<std::mutex> lg (listenerLock);
        listeners.add (listenerToAdd);
    }
}

void OscConnection::removeListener (OscConnection::Listener *listenerToRemove)
{
    if (listenerToRemove != nullptr)
    {
        std::lock_guard<std::mutex> lg (listenerLock);
        listeners.remove (listenerToRemove);
    }
}
