//
//  OscConnection.hpp
//  OscSerialTests
//
//  Created by Tom Mitchell on 28/06/2018.
//
//

#ifndef OscConnection_hpp
#define OscConnection_hpp

#include "../../../JuceLibraryCode/JuceHeader.h"
#include "OscMessage.h"

#ifdef JUCE_WINDOWS
#include <mutex>
#endif

/** Abstract base class for OSC connections */
 
class OscConnection
{
public:
    struct Settings
    {
        Settings() {}
        Settings (const IPAddress& ip, int s, int r) :  connectionType (Socket),
                                                        sendIp (ip),
                                                        sendPort (s),
                                                        receivePort (r) {}
        
        Settings (const std::string& pn) :   connectionType (Serial),
                                             portName (pn) {}

        enum Type
        {
            Socket,
            Serial,
        }connectionType {Socket};
        
        //socket
        IPAddress sendIp;
        int sendPort {0};
        int receivePort {0};

        //serial
        std::string portName;
        
        bool isSocketConnection() const {return connectionType == Socket;}
        bool isSerialConnection() const {return connectionType == Serial;}
        
        bool operator== (const Settings& other) const
        {
            return sendIp == other.sendIp
                   && sendPort == other.sendPort
                   && receivePort == other.receivePort
                   && portName == other.portName;
        }
        
        String toString() const
        {
            return isSocketConnection() ? sendIp.toString() + "\n" + String (sendPort) + "\n" + String (receivePort)
                                        : portName;
        }
    };
    
    enum class State
    {
        Disconnected,
        ConnectedSocket,
        ConnectedSerial
    };
    
    OscConnection() {};
    virtual ~OscConnection() {}
    
    virtual bool connect (const OscConnection::Settings& c) = 0;
    virtual void disconnect() = 0;
    
    /** sends a message  */
    virtual void send (const OscMessage& message) = 0;
    virtual void send (const OscBundle& bundle) = 0;
    
    /** Base class for recieving OSC messages on a serial port */
    class Listener
    {
    public:
        /** Destructor */
        virtual ~Listener() {}
        /** Called when the connection state changes, if there was an error that caused a state change a description will be in the errorMessage,
         an empty string indicates no error */
        virtual void oscConnectionStateChanged (OscConnection::State newState, const String& errorMessage) = 0;
        /** Called when an OSC message has been received */
        virtual void oscMessageReceived (const OscMessage& message) = 0;
    };
    
    /** Add a listener to this connection */
    void addListener (Listener* listener);
    
    /** Remove a listener from this connection */
    void removeListener (Listener* listener);
    
protected:
    std::mutex listenerLock;
    ListenerList<Listener> listeners;
};

#endif /* OscConnection_hpp */
